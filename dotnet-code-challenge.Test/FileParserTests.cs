using dotnet_code_challenge.Models;
using dotnet_code_challenge.Services;
using Xunit;

namespace dotnet_code_challenge.Test
{
    public class FileParserTests
    {
        [Fact]
        public void ShouldGetJsonFileParser()
        {
            var factory = new FileParserFactory();
            var instance = factory.GetInstance(FileTypes.json);
            Assert.NotNull(instance);
            Assert.IsType<JsonParser>(instance);
        }

        [Fact]
        public void ShouldGetXmlFileParser()
        {
            var factory = new FileParserFactory();
            var instance = factory.GetInstance(FileTypes.xml);
            Assert.NotNull(instance);
            Assert.IsType<XmlParser>(instance);
        }

        [Fact]
        public void ShouldParseXmlFile()
        {
            var fileParser = new XmlParser();
            var horses = fileParser.Parse(@"./FeedData/Caulfield_Race1.xml");

            Assert.NotNull(horses);
            Assert.Equal(2, horses.Count);
        }

        [Fact]
        public void ShouldParseJsonFile()
        {
            var fileParser = new JsonParser();
            var horses = fileParser.Parse(@"./FeedData/Wolferhampton_Race1.json");

            Assert.NotNull(horses);
            Assert.Equal(2, horses.Count);
        }
    }
}