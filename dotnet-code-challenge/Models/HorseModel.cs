﻿namespace dotnet_code_challenge.Models
{
    public class HorseModel
    {
        public string HorseName { get; set; }

        public double PrizeMoney { get; set; }
    }
}
