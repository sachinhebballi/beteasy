﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using dotnet_code_challenge.Models;
using dotnet_code_challenge.Services;

namespace dotnet_code_challenge
{
    class Program
    {
        static void Main(string[] args)
        {
            var files = Directory.GetFiles("./FeedData");
            var horsesList = new List<HorseModel>();
            foreach (var file in files)
            {
                var fileType = (FileTypes)Enum.Parse(typeof(FileTypes), Path.GetExtension(file).Substring(1));
                var fileFactory = new FileParserFactory().GetInstance(fileType);

                var horses = fileFactory.Parse(file);
                horsesList.AddRange(horses);
            }

            horsesList.OrderBy(_ => _.PrizeMoney).ToList().ForEach(_ => Console.WriteLine($"{_.HorseName}: {_.PrizeMoney}"));

            Console.ReadKey();
        }
    }
}
