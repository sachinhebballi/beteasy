﻿using dotnet_code_challenge.Models;

namespace dotnet_code_challenge.Services
{
    public class FileParserFactory
    {
        public IFileParser<HorseModel> GetInstance(FileTypes fileType)
        {
            IFileParser<HorseModel> fileParser = null;
            switch (fileType)
            {
                case FileTypes.json:
                    fileParser = new JsonParser();
                    break;
                case FileTypes.xml:
                    fileParser = new XmlParser();
                    break;
                default:
                    break;
            }

            return fileParser;
        }
    }
}