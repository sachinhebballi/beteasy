﻿using System.Collections.Generic;

namespace dotnet_code_challenge.Services
{
    public interface IFileParser<T> where T : class
    {
        List<T> Parse(string filePath);
    }
}
