﻿using dotnet_code_challenge.Models;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace dotnet_code_challenge.Services
{
    public class JsonParser : IFileParser<HorseModel>
    {
        public List<HorseModel> Parse(string filePath)
        {
            var jsonData = JsonConvert.DeserializeObject<RootObject>(File.ReadAllText(filePath));
            var horses = jsonData.RawData.Markets.SelectMany(_ => _.Selections.ToList()).Select(_ => new HorseModel
            {
                HorseName = _.Id,
                PrizeMoney = _.Price
            }).ToList();

            return horses;
        }
    }
}
