﻿using dotnet_code_challenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace dotnet_code_challenge.Services
{
    public class XmlParser : IFileParser<HorseModel>
    {
        public List<HorseModel> Parse(string filePath)
        {
            var xDoc = XDocument.Load(filePath);
            var horses = xDoc.XPathSelectElement("./meeting/races/race/horses").Elements("horse").Select(_ =>
                new HorseModel
                {
                    HorseName = _.Attribute("name")?.Value,
                    PrizeMoney = Convert.ToDouble(_.Element("prizemoney_won")?.Value)
                }).ToList();

            return horses;
        }
    }
}
